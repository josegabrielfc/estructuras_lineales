/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.colecciones_seed;

/**
 *
 * @author Jose Gabriel Fuentes - 1152085
 */
public class ListaCD<T> {

    private NodoD<T> cabecera;
    private int size;

    public ListaCD() {

        this.cabecera = new NodoD();
        this.cabecera.setInfo(null);
        this.cabecera.setSig(cabecera);
        this.cabecera.setAnt(cabecera);
        this.size = 0;
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabecera.getSig(), this.cabecera);
        this.cabecera.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.size++;

    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabecera, this.cabecera.getAnt());
        this.cabecera.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.size++;

    }

    private NodoD<T> getPos(int i) throws Exception {
        if (this.esVacia() || i < 0 || i >= this.size) {
            throw new Exception("Índice ilegal");
        }

        NodoD<T> aux = this.cabecera.getSig();
        while (i > 0) {
            aux = aux.getSig();
            i--;
        }
        return aux;
    }

    public T get(int i) {

        try {
            return this.getPos(i).getInfo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    public void set(int i, T infoNuevo) {
        try {
            this.getPos(i).setInfo(infoNuevo);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());

        }
    }

    @Override
    public String toString() {
        if (this.esVacia()) {
            return "Lista Vacía";
        }
        String msg = "Cab->";
        for (NodoD<T> x = this.cabecera.getSig(); x != this.cabecera; x = x.getSig()) {
            msg += x.getInfo() + "<-> ";
        }
        return msg + "<-> Cab";
    }

    public String toString_Inverso() {
        if (this.esVacia()) {
            return "Lista Vacía";
        }
        String msg = "Cab->";
        for (NodoD<T> x = this.cabecera.getAnt(); x != this.cabecera; x = x.getAnt()) {
            msg += x.getInfo() + "<-> ";
        }
        return msg + "<-> Cab";
    }

    public boolean esVacia() {
        return this.cabecera == this.cabecera.getSig();
        //this.size==0
        //this.cabecera==this.cabecera.getAnt();
    }

    public int getSize() {
        return size;
    }

    /**
     * Une dos listas en la original, usando teoría de conjuntos.La lista l2 se
     * borra. ejemplo: l1=<3,4,5,6> y l2=<3,4>
     * l1.unionLista(l2); l1=<3,4,5,6>
     * l2=<>
     *
     * El Metodo realiza la union de conjuntos de las 2 listas, pero no las
     * ordena, en la lista2 borra los elementos que se repiten con la lista 1,
     * luego de eliminar estos elementos la lista solo tendra elementos
     * diferentes a la lista 1, y se realizara la union de ambas listas, luego
     * se deja la lista 2 vacia.
     *
     * Como resultado: En la lista 1 quedaran todos los valores de la union de
     * conjuntos y la lista 2 quedara vacia
     *
     * @param l2
     */
    public void unionLista(ListaCD<T> l2) {

        findDuplicates(l2);

        if (!(l2.esVacia())) {

            NodoD<T> element = l2.cabecera.getSig();

            this.cabecera.getAnt().setSig(element);
            element.setAnt(this.cabecera.getAnt());

            l2.cabecera.getAnt().setSig(this.cabecera);
            this.cabecera.setAnt(l2.cabecera.getAnt());

            l2.cabecera.setSig(l2.cabecera); //Al realizar estas 2 lineas, la lista 2 queda vacia
            l2.cabecera.setAnt(l2.cabecera);
            l2.size = 0;
        }
        /*NodoD<T> element = l2.cabecera.getSig();       "Otra opción de agregar a la lista pero no es tan eficiente"

            while (element != l2.cabecera) {

                NodoD<T> nuevo = new NodoD(element.getInfo(), this.cabecera, this.cabecera.getAnt());

                this.cabecera.setAnt(nuevo);
                nuevo.getAnt().setSig(nuevo);
                this.size++;

                eliminarNodo(element);
                l2.size--;

                element = l2.cabecera.getSig();
                element = element.getSig();
                if (element.equals(l2.cabecera)) {
                    element = element.getSig();
                }
            }*/
    }

    /**
     * El metodo findDuplicates (Encontrar duplicados), elimina los datos
     * repetidos que se encuentran en ambas listas, pero solo los elimina en la
     * lista l2
     *
     * @param l2
     */
    private void findDuplicates(ListaCD<T> l2) {
        NodoD<T> element1 = this.cabecera.getSig();
        NodoD<T> element2 = l2.cabecera.getSig();

        while (element1 != this.cabecera) {
            while (element2 != l2.cabecera) {
                if (element1.getInfo().equals(element2.getInfo())) {

                    eliminarNodo(element2);
                    l2.size--;

                    element2 = l2.cabecera.getSig();
                }
                element2 = element2.getSig();

            }
            element1 = element1.getSig();
            element2 = l2.cabecera.getSig();
        }
    }

    /**
     * Metodo para eliminar un nodo en especifico
     *
     * @param temp
     */
    private void eliminarNodo(NodoD<T> temp) {

        temp.getAnt().setSig(temp.getSig());
        temp.getSig().setAnt(temp.getAnt());
        temp.setSig(null);
        temp.setAnt(null);

    }

    /**
     * Metodo que permite eliminar un elemento de la lista dada una posicion.
     *
     * @param i posicion del elemento
     * @return
     */
    public T borrar(int i) {

        try {
            NodoD<T> borrar = this.getPos(i);
            borrar.getAnt().setSig(borrar.getSig());
            borrar.getSig().setAnt(borrar.getAnt());
            borrar.setSig(null);
            borrar.setAnt(null);
            this.size--;
            return borrar.getInfo();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    /**
     * Concatena una lista en la original. La lista concatenada SE DEBE BORRAR.
     * NO SE PERMITE CREAR NODOS, LOS NODOS DEBEN SER REUTILIZADOS
     *
     * @param l2
     */
    public void concat(ListaCD<T> l2) {
        if (this.esVacia() || l2.esVacia()) {
            return;
        }
        if (this.esVacia() && !l2.esVacia()) {
            this.cabecera.setAnt(l2.cabecera.getAnt());
            this.cabecera.setSig(l2.cabecera.getSig());
            l2.cabecera.getSig().setAnt(this.cabecera);
            l2.cabecera.getAnt().setSig(this.cabecera);
            this.size = l2.size;
            l2.size = 0;
            this.desUnir(l2.cabecera);
            return;
        }
        if (!this.esVacia() && !l2.esVacia()) {
            NodoD<T> l2Inicio = l2.cabecera.getSig();
            NodoD<T> l2Fin = l2.cabecera.getSig();
            this.cabecera.getAnt().setSig(l2Inicio);
            l2Inicio.setAnt(this.cabecera.getAnt());
            this.cabecera.setAnt(l2Fin);
            l2Fin.setSig(this.cabecera);
            this.size += l2.size;
            l2.size = 0;
            this.desUnir(l2.cabecera);
        }
    }

    /**
     * El Metodo Concatena la lista l2 en la original a partir de la posición
     * indicada por i (después de i) l2 al final del proceso queda vacía
     *
     * @param i posición donde va a insertar l2
     * @param l2 lista a ser concatenadas
     */
    public void concat(int i, ListaCD<T> l2) {
        if (this.esVacia() || l2.esVacia()) {
            return;
        }
        try {
            NodoD<T> aux = this.getPos(i);
            NodoD<T> firstElement = l2.cabecera.getSig();
            NodoD<T> lastElement = l2.cabecera.getAnt();

            firstElement.setAnt(aux);
            lastElement.setSig(aux.getSig());
            aux.getSig().setAnt(lastElement);
            aux.setSig(firstElement);

            this.size += l2.size;
            l2.size = 0;
            desUnir(l2.cabecera);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void desUnir(NodoD<T> nodo) {
        nodo.setAnt(nodo);
        nodo.setSig(nodo);
    }

    public void borrarTodo() {
        this.cabecera.setInfo(null);
        this.cabecera.setSig(cabecera);
        this.cabecera.setAnt(cabecera);
        this.size = 0;
    }

}
