/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import util.colecciones_seed.ListaCD;

/**
 *
 * @author Jose Gabriel Fuentes - 1152085
 */
public class TestListaCD {

    public static void main(String[] args) {
        ListaCD<String> l = new ListaCD();
        l.insertarInicio("Heydi");
        l.insertarInicio("Maiken");
        l.insertarInicio("Javier");
        l.insertarInicio("Daniela");
        l.insertarInicio("Saray");
        System.out.println(l.toString());

        ListaCD<Integer> l2 = new ListaCD();
        l2.insertarInicio(8);
        l2.insertarInicio(3);
        l2.insertarInicio(5);
        l2.insertarInicio(2);
        l2.insertarInicio(7);
        System.out.println("Lista 2: " + l2.toString());

        ListaCD<Integer> l3 = new ListaCD();
        l3.insertarInicio(7);
        l3.insertarInicio(1);
        l3.insertarInicio(5);
        l3.insertarInicio(4);
        l3.insertarInicio(9);
        l3.insertarInicio(5);

        System.out.println("Lista 3: " + l3.toString());

        /*l2.unionLista(l3);
        System.out.println("Al realizar la union de conjuntos de la Lista2 y Lista3 es: \nLista 2: " + l2.toString());
        System.out.println("Lista 3: " + l3.toString());

        System.out.println("Borrando a: " + l.borrar(0));
        System.out.println(l.toString());*/
        int n = 2; // Modificar la posicion aqui, par que la posicion sea imprimida

        l2.concat(n, l3);
        System.out.println("Al unir Lista 2 y Lista 3, en la posicion " + n +"\nLa lista 2 queda: "+ l2.toString());
        System.out.println("Lista 3: " + l3.toString());

        //ESTO ES LA FORMA CANDIDA :(
        /*for(int i=0;i<l.getSize();i++)
            System.out.println("Elemento -"+i+"\t:"+l.get(i));*/
    }
}
